import itertools


def get_minimum_connections(matrix):
    connected = {}
    for i, v in itertools.product(range(len(matrix)), range(len(matrix))):
        if matrix[i][v] and v not in connected:
            if i not in connected:
                connected.setdefault(i, [])
            connected[i] += [v]

    return len(connected) - 1


matrix = [
    [False, True, False, False, True],
    [True, False, False, False, False],
    [False, False, False, True, False],
    [False, False, True, False, False],
    [True, False, False, False, False],
]

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print(get_minimum_connections(matrix))  # should print 1

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
